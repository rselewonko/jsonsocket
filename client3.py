import socket

PORT = 7070
SERVER = '127.0.0.1'
ADDR = (SERVER, PORT)

HEADER = 64
FORMAT = 'utf-8'

with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as c:
    c.connect(ADDR)
    # print("WELCOME TO THE SERVER")
    # welcomeMessage = c.recv(1024).decode(FORMAT)
    print(c.recv(1024).decode(FORMAT))

    while True:
        msg = input('>>> ').encode(FORMAT)
        c.send(msg)

        data = c.recv(1024).decode(FORMAT)
        if data == "STOP" or not data:
            print(data)
            break
        else:
            print('[SERVER] %s' % data)