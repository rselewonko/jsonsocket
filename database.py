import json


class Database():
    # def __init__(self):
    #     f = open('database.json')
    #     self.data = json.load(f)
    #     #self.data = DataBase.up()

    # def show_data(self):
    #     # f = open('database.json')
    #     # data = json.load(f)
    #     return self.data

    def update_data(self):
        f = open('database.json')
        self.data2 = json.load(f)
        print(self.data2)
        return self.data2

    def newUser(self, userObject):
        with open('database.json', 'r+') as file:
            data = json.load(file)
            print(userObject.username)
            if not self.check_if_user_exist(userObject.username):
                data['USERS'].append(
                    {'username': userObject.username, 'password': userObject.password, 'rights': 0, 'box': userObject.box})
                file.seek(0)
                json.dump(data, file, indent=4)
                return True
            return False

        # print('In my_method method of testMethod from Database')
        # print("Name:", userObject.username)
        # print("Age:", userObject.password)

    def check_if_user_exist(self, username):
        with open('database.json', 'r+') as file:
            data = json.load(file)
            for user in data['USERS']:
                if username == user['username']:
                    return user
            return False

    def check_if_password_correct(self, username, password):
        with open('database.json', 'r+') as file:
            data = json.load(file)
            for user in data['USERS']:
                if username == user['username'] and password == user['password']:
                    return True
            return False

    def check_if_admin(self, username, rights):
        with open('database.json', 'r+') as file:
            data = json.load(file)
            for user in data['USERS']:
                if username == user['username'] and rights == user['rights']:
                    return True
            return False

    def remove_account(self, username):
        with open('database.json', 'r+') as file:
            data = json.load(file)
            user = self.check_if_user_exist(username)
            # for user in data['USERS']:
            if user:
                data['USERS'].remove(user)
                file.seek(0)
                json.dump(data, file, indent=4)
                file.truncate()
                return True
            return False
            # return None
            # data = json.load(data)
            # username['rights'])
            # if username['rights'] == rights:
            #     return True
            # return False

    def change_password(self, username, oldPassword, newPassword):
        with open('database.json', 'r+') as file:
            data = json.load(file)
            for user in data['USERS']:
                if user['username'] == username and user['password'] == oldPassword:
                    # print(user['password'])
                    user['password'] = newPassword
                    file.seek(0)
                    json.dump(data, file, indent=4)
                    file.truncate()
                    print('metoda sie wykonala')
                    return True
            return False

    def send_message(self, sender, receiver, message):
        with open('database.json', 'r+') as file:
            data = json.load(file)
            for user in data['USERS']:
                if user['username'] == receiver and user['rights'] == 0 and len(user['box']) < 5:
                    msg_len = len(user['box']) + 1
                    user['box'][f"{msg_len}. {sender}"] = message[:255]
                    print(user['box'])
                    file.seek(0)
                    json.dump(data, file, indent=4)
                    file.truncate()
                    return True
                elif user['username'] == receiver and user['rights'] == 1:
                    msg_len = len(user['box']) + 1
                    user['box'][f"{msg_len}. {sender}"] = message[:255]
                    file.seek(0)
                    json.dump(data, file, indent=4)
                    file.truncate()
                    return True
            return False

    def show_messages(self, username):
        with open('database.json', 'r+') as file:
            data = json.load(file)
            user = self.check_if_user_exist(username)
            if user:
                return user['box']
            return False

    def clear_messages(self, admin, username):
        with open('database.json', 'r+') as file:
            data = json.load(file)
            for user in data['USERS']:
                if user['username'] == username:
                    user['box'].clear()
                    file.seek(0)
                    json.dump(data, file, indent=4)
                    file.truncate()
                    return True
            return False


db = Database()

# db.clear_messages("Kuba", "Ania")
# print(db.show_messages('Ania'))
# db.send_message('dupa', 'Kuba', 'wiadomosc')
# authorization = input('podaj').split(' ')
# user = User(authorization[0], authorization[1], 0)
# db.newUser(user)
# db.change_password('Ania', 'noweHaslo2')
# db.update_data()
# db.remove_account('nowy22')
# db.update_data()
# print(db.check_if_user_exist('Pawel'))
# print(db.check_if_admin('Kuba', 1))
# print(db.check_if_password_correct('Kuba', 'kuba'))
# if db.check_if_user_exist('Pawel'):
#     print('prawda')
# else:
#     print('dupa')
# db.check_if_password_correct('sawel')

# if db.check_if_admin('USER'):
#     print('prawda')
# else:
#     print('dupa')

# db.register_user()

# def testMethod(self, userObject):
#     print('In my_method method of testMethod from Database')
#     print("Name:", userObject.username)
#     print("Age:", userObject.password)
#     print(type(self.data['USERS']))
#     self.data['USERS'].append({'username': 'dupa'})
#     #json.dump(self.data, f, indent=4)

# def register_user(self, userObject):
#     index = self.check_if_user_exist(userObject["username"])
#     if index == None:  # if username not exist in DB
#         return self.data["USERS"].append(userObject)
#     return None

# def testMethod(self, userObject):
#     with open('database.json', 'r+') as f:
#         data = json.load(f)
#         data['USERS'].append({'username':userObject.username, 'password':userObject.password}) # <--- add user to jsonfile.
#         f.seek(0)        # <--- should reset file position to the beginning.
#         json.dump(data, f, indent=4)
#     print('In my_method method of testMethod from Database')
#     print("Name:", userObject.username)
#     print("Age:", userObject.password)
